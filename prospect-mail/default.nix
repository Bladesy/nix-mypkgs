{ pkgs ? import <nixpkgs> {} }:

with pkgs;
appimageTools.wrapType2 rec {
	name 	= "prospect-mail";
	version = "0.4.0";

  	src 	= fetchurl {
		url 	= "https://github.com/julian-alarcon/prospect-mail/releases/download/v${version}/Prospect-Mail-${version}.AppImage";
		sha256 	= "f02340fc934a68dbff9e9f965969bf89085955dd56ea0bb654c58fe536857141";
  	};
}
