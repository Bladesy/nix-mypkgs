unset PATH
for buildInput in ${buildInputs}
do
	export PATH="${PATH}${PATH:+:}${buildInput}/bin"
done

tar -xzf ${src}
mkdir -p ${out}/share
cp -r ubuntu-mate-artwork-${version}/usr/share/{icons,themes} ${out}/share
for iconTheme in ${out}/share/icons/*
do
	gtk-update-icon-cache ${iconTheme}
done
