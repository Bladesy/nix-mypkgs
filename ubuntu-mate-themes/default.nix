{ pkgs ? import <nixpkgs> {} }:

with pkgs;
derivation rec {
	name 	= "ubuntu-mate-themes-${version}";
	version = "22.04.2";
	system 	= builtins.currentSystem;

	builder 	= "${bash}/bin/bash";
	args 		= [ ./builder.sh ];
	buildInputs 	= [ coreutils gnutar gzip gtk3 ];

	src 	= fetchurl {
		url 	= "https://github.com/ubuntu-mate/ubuntu-mate-artwork/archive/refs/tags/${version}.tar.gz";
		sha256 	= "94d8cc15a9bc52852422090bdc1c6ca0f648bdc48f8f1c6566037ee700e95042";
	};
}
